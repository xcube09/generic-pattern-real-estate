﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GenericPattern.Service
{
    public interface IService<TEntity> where TEntity: class
    {
        TEntity Find(params object[] keyValues);

        void Insert(TEntity entity);

        void InsertRange(IEnumerable<TEntity> entities);

        void Update(TEntity entity);

        void Delete(object id);

        void Delete(TEntity entity);

        IEnumerable<TEntity> SelectPage(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, int page,
            int pageSize, out int totalCount);

        IEnumerable<TEntity> Select(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null, int? page = null,
            int? pageSize = null);
    }
}