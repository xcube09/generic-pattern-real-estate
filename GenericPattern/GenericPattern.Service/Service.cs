﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using GenericPattern.Repository;

namespace GenericPattern.Service
{
    public class Service<TEntity> : IService<TEntity> where TEntity : class
    {
        private readonly IRepository<TEntity> _repository;

        public Service(IRepository<TEntity> repository)
        {
            _repository = repository;
        }

        public TEntity Find(params object[] keyValues)
        {
            return _repository.Find(keyValues);
        }

        public void Insert(TEntity entity)
        {
            _repository.Insert(entity);
        }

        public void InsertRange(IEnumerable<TEntity> entities)
        {
            _repository.InsertRange(entities);
        }

        public void Update(TEntity entity)
        {
            _repository.Update(entity);
        }

        public void Delete(object id)
        {
            _repository.Delete(id);
        }

        public void Delete(TEntity entity)
        {
            _repository.Delete(entity);
        }

        public IEnumerable<TEntity> SelectPage(Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy, int page, int pageSize, out int totalCount)
        {
            totalCount = _repository.Select().Count();
            return _repository.Select(orderBy: orderBy, page: page, pageSize: pageSize).AsEnumerable();
        }

        public IEnumerable<TEntity> Select(Expression<Func<TEntity, bool>> filter = null, 
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null, 
            List<Expression<Func<TEntity, object>>> includes = null, int? page = null,
            int? pageSize = null)
        {
            return _repository.Select(filter, orderBy, includes, page, pageSize).AsEnumerable();
        }
    }
}