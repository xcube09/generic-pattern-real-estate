﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace GenericPattern.Repository
{
    public interface IRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// searches the underlying context for items with the provided keys
        /// </summary>
        /// <param name="keyValues"></param>
        /// <returns>returns a single item or a collection</returns>
        TEntity Find(params object[] keyValues);

        /// <summary>
        /// inserts an entity
        /// </summary>
        /// <param name="entity"></param>
        void Insert(TEntity entity);

        /// <summary>
        /// inserts a collection range
        /// </summary>
        /// <param name="entities"></param>
        void InsertRange(IEnumerable<TEntity> entities);

        /// <summary>
        /// updates an entity
        /// </summary>
        /// <param name="entity"></param>
        void Update(TEntity entity);

        /// <summary>
        /// deletes an entity with a given id
        /// </summary>
        /// <param name="id"></param>
        void Delete(object id);

        /// <summary>
        /// deletes a given entity
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);

        IRepository<T> GetRepository<T>() where T : class;

        /// <summary>
        /// Returns an iqueryaable
        /// </summary>
        /// <returns></returns>
        IQueryable<TEntity> Select(Expression<Func<TEntity, bool>> filter = null,
            Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null,
            List<Expression<Func<TEntity, object>>> includes = null,
            int? page = null,
            int? pageSize = null);
    }
}
