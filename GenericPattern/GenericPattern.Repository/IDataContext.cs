﻿using System;

namespace GenericPattern.Repository
{
    public interface IDataContext : IDisposable
    {
        /// <summary>
        /// persist all items which have been modified or added to the database
        /// </summary>
        /// <returns>returns the number of rows affected in the database</returns>
        int SaveChanges();
    }
}