﻿using System;

namespace GenericPattern.Repository
{
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>
        /// commits all changes to the database
        /// </summary>
        /// <returns>returns database affected rows count</returns>
        int SaveChanges();

        IRepository<TEntity> Repository<TEntity>() where TEntity : class;
    }
}