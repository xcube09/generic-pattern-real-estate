﻿using GenericPattern.Service;
using RealEstate.Models;

namespace RealEstate.Service
{
    public interface ICustomerService : IService<Customer>
    {

    }
}