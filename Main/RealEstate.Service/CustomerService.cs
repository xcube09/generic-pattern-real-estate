﻿using GenericPattern.Repository;
using GenericPattern.Service;
using RealEstate.Models;

namespace RealEstate.Service
{
    public class CustomerService : Service<Customer>, ICustomerService
    {
        public CustomerService(IRepository<Customer> repository) : base(repository)
        {
        }
    }
}