﻿CREATE TABLE [dbo].[Orders] (
    [Id]          INT          IDENTITY (1, 1) NOT NULL,
    [Quantity]    INT          NOT NULL,
    [Total]       DECIMAL (18) NOT NULL,
    [Customer_Id] INT          NOT NULL,
    CONSTRAINT [PK_Orders] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_CustomerOrder] FOREIGN KEY ([Customer_Id]) REFERENCES [dbo].[Customers] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_CustomerOrder]
    ON [dbo].[Orders]([Customer_Id] ASC);

