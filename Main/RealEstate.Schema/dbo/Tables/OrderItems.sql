﻿CREATE TABLE [dbo].[OrderItems] (
    [Id]          INT            NOT NULL,
    [Description] NVARCHAR (MAX) NULL,
    [Order_Id]    INT            NOT NULL,
    CONSTRAINT [PK_OrderItems] PRIMARY KEY CLUSTERED ([Id] ASC),
    CONSTRAINT [FK_OrderOrderItem] FOREIGN KEY ([Order_Id]) REFERENCES [dbo].[Orders] ([Id])
);


GO
CREATE NONCLUSTERED INDEX [IX_FK_OrderOrderItem]
    ON [dbo].[OrderItems]([Order_Id] ASC);

