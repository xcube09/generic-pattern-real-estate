using System.Web.Mvc;
using AutoMapper;
using Microsoft.Practices.Unity;
using Unity.Mvc5;
using GenericPattern.Repository;
using RealEstate.Models;
using GenericPattern.Repository.Ef6;
using GenericPattern.Service;
using RealEstate.Service;

namespace RealEstate.Web
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IDataContext, ApplicationModelContainer>(new PerResolveLifetimeManager());
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>));
            container.RegisterType<IUnitOfWork, UnitOfWork>();
            container.RegisterType(typeof(Service<>));
            container.RegisterType<ICustomerService, CustomerService>();
            container.RegisterInstance<IMapper>(AutoMapperConfig.Configure());

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}