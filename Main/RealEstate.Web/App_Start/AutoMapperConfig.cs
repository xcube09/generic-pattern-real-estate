﻿using AutoMapper;
using RealEstate.Models;
using RealEstate.Web.ViewModels;

namespace RealEstate.Web
{
    public static class AutoMapperConfig
    {
        public static IMapper Configure()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Customer, CustomerViewModel>().ReverseMap();
            });

            return config.CreateMapper();
        }
    }
}