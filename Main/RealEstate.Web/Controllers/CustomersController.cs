﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using GenericPattern.Repository;
using RealEstate.Models;
using RealEstate.Service;
using RealEstate.Web.ViewModels;

namespace RealEstate.Web.Controllers
{
    public class CustomersController : BaseController
    {
        private readonly ICustomerService _customerService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public CustomersController(ICustomerService customerService, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _customerService = customerService;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        // GET: Customers
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public JsonResult GetCustomers(DataTableAjaxPostModel model)
        {
            int totalResultsCount;
            var customers = _customerService.SelectPage(x => x.OrderBy(y => y.Id), 1, PageSize, out totalResultsCount);
            var viewModels = customers.Select(_mapper.Map<CustomerViewModel>).ToList();
            return Json(new
            {
                draw = model.Draw,
                recordsTotal = totalResultsCount,
                recordsFiltered = totalResultsCount,
                data = viewModels
            });
        }

        public JsonResult GetCustomer(int id)
        {
            var customer = _customerService.Find(id);
            return Json(_mapper.Map<CustomerViewModel>(customer), JsonRequestBehavior.AllowGet);
        }
        

        // POST: Customers/Create
        [HttpPost]
        public ActionResult Create(CustomerViewModel model)
        {
            var customer = _mapper.Map<Customer>(model);
            _customerService.Insert(customer);

            _unitOfWork.SaveChanges();

            return Json(_mapper.Map<CustomerViewModel>(customer));
        }
        

        // POST: Customers/Edit/5
        [HttpPost]
        public ActionResult Edit(CustomerViewModel model)
        {
            var customer = _mapper.Map<Customer>(model);
            _customerService.Update(customer);

            _unitOfWork.SaveChanges();

            return Json(_mapper.Map<CustomerViewModel>(customer));
        }

        // GET: Customers/Delete/5
        public ActionResult Delete(int id)
        {
            _customerService.Delete(id);
            _unitOfWork.SaveChanges();

            return Json("", JsonRequestBehavior.AllowGet);
        }
        
    }
}
