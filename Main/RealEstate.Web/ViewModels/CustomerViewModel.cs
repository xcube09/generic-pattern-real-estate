﻿using System;

namespace RealEstate.Web.ViewModels
{
    public class CustomerViewModel
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime DateOfBirth { get; set; }

        public string DateOfBirthString => DateOfBirth.ToString("d");
    }
}